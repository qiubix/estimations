// root

tasks.register<Zip>("zipSources") {
  val archiveName = "sources.zip"
  val jarName = "api-0.0.1-SNAPSHOT.jar"
  delete(archiveName)

  archiveFileName.set(archiveName)
  destinationDirectory.set(layout.projectDirectory)

  from(layout.projectDirectory.file("/api/build/libs/$jarName"))
  from(layout.projectDirectory) {
    include(
      "Buildfile",
      "Procfile",
      "build.gradle.kts",
      "settings.gradle.kts",
      "gradle/**",
      "gradlew",
      "gradlew.bat",
      jarName,
      "system.properties",
      "api/**",
      "ui/**"
    )
    exclude(
      "api/build/**",
      "ui/.gradle/**",
      "ui/dist/**",
      "ui/node/**",
      "ui/node_modules/**",
      "**/*.zip",
      "**/*.csv"
    )
  }
}
