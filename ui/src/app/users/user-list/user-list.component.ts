import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../users.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  displayedColumns = ['username', 'active', 'options'];
  dataSource: MatTableDataSource<User> = new MatTableDataSource<User>();
  private _users: User[] = [];

  @Input()
  set users(users: User[]) {
    this.dataSource.data = users;
    this._users = users;
  }

  @Output() toggleActive: EventEmitter<User> = new EventEmitter<User>();

  constructor() {}

  ngOnInit(): void {}

  get users(): User[] {
    return this._users;
  }

  toggleActiveUser(user: User) {
    this.toggleActive.emit(user);
  }
}
