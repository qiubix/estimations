import { Action } from '@ngrx/store';
import { User } from '../users.model';

export namespace UserActions {
  export const types = {
    LoadList: '[users] load list',
    LoadListSuccess: '[users] load list success',
    Login: '[users] login',
    LoginSuccess: '[users] login success',
    Register: '[users] register',
    RegisterSuccess: '[users] register success',
    ToggleActive: '[users] toggle active',
    ToggleActiveSuccess: '[users] toggle active success',
    DeleteUser: '[users] delete',
    DeleteUserSuccess: '[users] delete success',
    Fail: '[Users] action failed',
  };

  export class LoadList implements Action {
    type = types.LoadList;
    constructor(public payload?: any) {}
  }

  export class LoadListSuccess implements Action {
    type = types.LoadListSuccess;
    constructor(public payload?: User[]) {}
  }

  export class Login implements Action {
    type = types.Login;
    constructor(public payload: string) {}
  }

  export class LoginSuccess implements Action {
    type = types.LoginSuccess;
    constructor(public payload: User) {}
  }

  export class Register implements Action {
    type = types.Register;
    constructor(public payload: string) {}
  }

  export class RegisterSuccess implements Action {
    type = types.RegisterSuccess;
    constructor(public payload: User) {}
  }

  export class ToggleActive implements Action {
    type = types.ToggleActive;
    constructor(public payload: User) {}
  }

  export class ToggleActiveSuccess implements Action {
    type = types.ToggleActiveSuccess;
    constructor(public payload: User) {}
  }

  export class DeleteUser implements Action {
    type = types.DeleteUser;
    constructor(public payload: string) {}
  }

  export class DeleteUserSuccess implements Action {
    type = types.DeleteUserSuccess;
    constructor(public payload?: any) {}
  }

  export class Fail implements Action {
    type = types.Fail;
    constructor(public payload?: any) {}
  }
}
