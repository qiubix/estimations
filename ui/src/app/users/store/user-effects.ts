import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { UsersService } from '../users.service';
import { UserActions } from './user-actions';

import LoadListSuccess = UserActions.LoadListSuccess;
import DeleteUserSuccess = UserActions.DeleteUserSuccess;
import LoginSuccess = UserActions.LoginSuccess;
import RegisterSuccess = UserActions.RegisterSuccess;
import ToggleActiveSuccess = UserActions.ToggleActiveSuccess;
import FailedAction = UserActions.Fail;

@Injectable()
export class UsersEffects {
  @Effect()
  LoadAllUsers: Observable<Action> = this.actions.pipe(
    ofType(UserActions.types.LoadList),
    switchMap(() =>
      this.service.getAll().pipe(
        map((data) => new LoadListSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  Login: Observable<Action> = this.actions.pipe(
    ofType(UserActions.types.Login),
    switchMap((action: UserActions.Login) =>
      this.service.login(action.payload).pipe(
        map((data) => new LoginSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  Register: Observable<Action> = this.actions.pipe(
    ofType(UserActions.types.Register),
    switchMap((action: UserActions.Register) =>
      this.service.register(action.payload).pipe(
        map((data) => new RegisterSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  ToggleActive: Observable<Action> = this.actions.pipe(
    ofType(UserActions.types.ToggleActive),
    switchMap((action: UserActions.ToggleActive) =>
      this.service.toggleActive(action.payload).pipe(
        map((data) => new ToggleActiveSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  DeleteOne: Observable<Action> = this.actions.pipe(
    ofType(UserActions.types.DeleteUser),
    switchMap((action: UserActions.DeleteUser) =>
      this.service.delete(action.payload).pipe(
        map((data) => new DeleteUserSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  constructor(private actions: Actions, private service: UsersService) {}
}
