import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './core/auth/auth.guard';
import { MainComponent } from './core/main/main.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: '',
        component: MainComponent,
        canActivateChild: [AuthGuard],
        children: [
          {
            path: 'tasks',
            loadChildren: './tasks/tasks.module#TasksModule',
          },
          {
            path: 'estimations',
            loadChildren: './estimations/estimations.module#EstimationsModule',
          },
          {
            path: 'users',
            loadChildren: './users/users.module#UsersModule',
          },
          {
            path: '',
            redirectTo: 'tasks',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
      },
      {
        path: 'auth',
        loadChildren: './core/auth/auth.module#AuthModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
