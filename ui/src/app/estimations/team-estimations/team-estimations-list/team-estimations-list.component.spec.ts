import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEstimationsListComponent } from './team-estimations-list.component';

describe('TeamEstimationsListComponent', () => {
  let component: TeamEstimationsListComponent;
  let fixture: ComponentFixture<TeamEstimationsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TeamEstimationsListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEstimationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
