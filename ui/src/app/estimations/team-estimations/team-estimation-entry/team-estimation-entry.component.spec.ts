import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEstimationEntryComponent } from './team-estimation-entry.component';

describe('TeamEstimationEntryComponent', () => {
  let component: TeamEstimationEntryComponent;
  let fixture: ComponentFixture<TeamEstimationEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TeamEstimationEntryComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEstimationEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
