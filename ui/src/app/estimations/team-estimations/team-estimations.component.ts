import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { IndividualEstimation } from '../estimations.model';
import { EstimationsActions } from '../store/estimations-actions';
import { Estimations } from '../store/estimations-reducer';

@Component({
  selector: 'app-team-estimations',
  templateUrl: './team-estimations.component.html',
  styleUrls: ['./team-estimations.component.css'],
})
export class TeamEstimationsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  estimations$: Observable<IndividualEstimation[]>;
  teamReady$: Observable<boolean>;

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new EstimationsActions.GetCurrentUser());
    this.store.dispatch(new EstimationsActions.LoadTeamEstimations());
    this.estimations$ = this.store.pipe(select(AppSelectors.getTeamEstimationsList));
    this.teamReady$ = this.store.pipe(select(AppSelectors.getWholeTeamReady));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  reset(): void {
    this.store.dispatch(new EstimationsActions.ResetAll());
  }

  refresh(): void {
    this.store.dispatch(new EstimationsActions.LoadTeamEstimations());
  }
}
