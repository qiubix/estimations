import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEstimationsComponent } from './team-estimations.component';

describe('TeamEstimationsComponent', () => {
  let component: TeamEstimationsComponent;
  let fixture: ComponentFixture<TeamEstimationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TeamEstimationsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEstimationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
