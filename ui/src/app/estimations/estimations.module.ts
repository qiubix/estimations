import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstimationsComponent } from './root/estimations.component';
import { EstimationsRoutingModule } from './estimations-routing.module';
import { OwnEstimationsComponent } from './own-estimations/own-estimations.component';
import { TeamEstimationsComponent } from './team-estimations/team-estimations.component';
import { AvailableTasksListComponent } from './own-estimations/available-tasks-list/available-tasks-list.component';
import { PickedTasksListComponent } from './own-estimations/picked-tasks-list/picked-tasks-list.component';
import { OwnSummaryComponent } from './own-estimations/own-summary/own-summary.component';
import { SummaryComponent } from './summary/summary.component';
import { TeamEstimationsListComponent } from './team-estimations/team-estimations-list/team-estimations-list.component';
import { TeamEstimationEntryComponent } from './team-estimations/team-estimation-entry/team-estimation-entry.component';
import { SharedModule } from '../shared/shared.module';
import { EstimationsService } from './estimations.service';
import { StatsComponent } from './summary/stats/stats.component';
import { PicksSummaryTableComponent } from './summary/picks-summary-table/picks-summary-table.component';

@NgModule({
  declarations: [
    EstimationsComponent,
    OwnEstimationsComponent,
    TeamEstimationsComponent,
    AvailableTasksListComponent,
    PickedTasksListComponent,
    PicksSummaryTableComponent,
    OwnSummaryComponent,
    SummaryComponent,
    TeamEstimationsListComponent,
    TeamEstimationEntryComponent,
    StatsComponent,
  ],
  imports: [SharedModule, EstimationsRoutingModule],
  exports: [EstimationsComponent],
  providers: [EstimationsService],
})
export class EstimationsModule {}
