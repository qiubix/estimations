import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PicksSummaryTableComponent } from './picks-summary-table.component';

describe('PicksSummaryTableComponent', () => {
  let component: PicksSummaryTableComponent;
  let fixture: ComponentFixture<PicksSummaryTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PicksSummaryTableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PicksSummaryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
