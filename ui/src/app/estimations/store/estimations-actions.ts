import { Action } from '@ngrx/store';
import { Task } from 'src/app/tasks/tasks.model';
import { IndividualEstimation, TeamSummary } from '../estimations.model';

export namespace EstimationsActions {
  export const types = {
    LoadPickedTasks: '[Estimations] Load picked tasks',
    LoadPickedTasksSuccess: '[Estimations] Load picked tasks success',
    LoadAvailableTasks: '[Estimations] Load available tasks',
    LoadAvailableTasksSuccess: '[Estimations] Load available tasks success',
    Pick: '[Estimations] Pick task',
    PickSuccess: '[Estimations] Pick task success',
    RemovePick: '[Estimations] Remove picked task',
    RemovePickSuccess: '[Estimations] Remove picked task success',
    LoadTeamEstimations: '[Estimations] Load team estimations',
    LoadTeamEstimationsSuccess: '[Estimations] Load team estimations success',
    SubmitEstimation: '[Estimations] Submit estimations',
    SubmitEstimationSuccess: '[Estimations] Submit estimations success',
    UpdateEstimation: '[Estimations] Update estimation',
    UpdateEstimationSuccess: '[Estimations] Update estimation success',
    ResetSubmission: '[Estimations] Reset submission',
    ResetSubmissionSuccess: '[Estimations] Reset submission success',
    GetCurrentUser: '[Estimations] Get current user',
    GetCurrentUserSuccess: '[Estimations] Get current user success',
    ResetAll: '[Estimations] Reset all',
    ResetAllSuccess: '[Estimations] Reset all success',
    CheckTeamReady: '[Estimations] Check if team is ready',
    CheckTeamReadySuccess: '[Estimations] Check if team is ready success',
    GetSummary: '[Estimations] Get summary',
    GetSummarySuccess: '[Estimations] Get summary success',
    Fail: '[Estimations] Action failed',
  };

  export class LoadPickedTasks implements Action {
    type = types.LoadPickedTasks;
    constructor(public payload?: any) {}
  }

  export class LoadPickedTasksSuccess implements Action {
    type = types.LoadPickedTasksSuccess;
    constructor(public payload: Task[]) {}
  }

  export class LoadAvailableTasks implements Action {
    type = types.LoadAvailableTasks;
    constructor(public payload?: any) {}
  }

  export class LoadAvailableTasksSuccess implements Action {
    type = types.LoadAvailableTasksSuccess;
    constructor(public payload: Task[]) {}
  }

  export class Pick implements Action {
    type = types.Pick;
    constructor(public payload: Task) {}
  }

  export class PickSuccess implements Action {
    type = types.PickSuccess;
    constructor(public payload: Task) {}
  }

  export class RemovePick implements Action {
    type = types.RemovePick;
    constructor(public payload: Task) {}
  }

  export class RemovePickSuccess implements Action {
    type = types.RemovePickSuccess;
    constructor(public payload: Task) {}
  }

  export class LoadTeamEstimations implements Action {
    type = types.LoadTeamEstimations;
    constructor(public payload?: any) {}
  }

  export class LoadTeamEstimationsSuccess implements Action {
    type = types.LoadTeamEstimationsSuccess;
    constructor(public payload: IndividualEstimation[]) {}
  }

  export class SubmitEstimation implements Action {
    type = types.SubmitEstimation;
    constructor(public payload: number[]) {}
  }

  export class SubmitEstimationSuccess implements Action {
    type = types.SubmitEstimationSuccess;
    constructor(public payload: IndividualEstimation) {}
  }

  export class ResetSubmission implements Action {
    type = types.ResetSubmission;
    constructor(public payload?: any) {}
  }

  export class ResetSubmissionSuccess implements Action {
    type = types.ResetSubmissionSuccess;
    constructor(public payload: any) {}
  }

  export class ResetAll implements Action {
    type = types.ResetAll;
    constructor(public payload?: any) {}
  }

  export class ResetAllSuccess implements Action {
    type = types.ResetAllSuccess;
    constructor(public payload: any) {}
  }

  export class UpdateEstimation implements Action {
    type = types.UpdateEstimation;
    constructor(public payload: number[]) {}
  }

  export class UpdateEstimationSuccess implements Action {
    type = types.UpdateEstimationSuccess;
    constructor(public payload: IndividualEstimation) {}
  }

  export class GetCurrentUser implements Action {
    type = types.GetCurrentUser;
    constructor(public payload?: any) {}
  }

  export class GetCurrentUserSuccess implements Action {
    type = types.GetCurrentUserSuccess;
    constructor(public payload: string) {}
  }

  export class CheckTeamReady implements Action {
    type = types.CheckTeamReady;
    constructor(public payload?: any) {}
  }

  export class CheckTeamReadySuccess implements Action {
    type = types.CheckTeamReadySuccess;
    constructor(public payload: boolean) {}
  }

  export class GetSummary implements Action {
    type = types.GetSummary;
    constructor(public payload?: any) {}
  }

  export class GetSummarySuccess implements Action {
    type = types.GetSummarySuccess;
    constructor(public payload: TeamSummary) {}
  }

  export class Fail implements Action {
    type = types.Fail;
    constructor(public payload: any) {}
  }
}
