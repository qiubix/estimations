import { Category, Task } from 'src/app/tasks/tasks.model';
import { IndividualEstimation, TeamSummary } from '../estimations.model';
import { EstimationsActions } from './estimations-actions';

export namespace Estimations {
  export interface State {
    pickedTasks: Task[];
    availableTasks: Task[];
    teamEstimations: IndividualEstimation[];
    ownEstimationSubmitted: boolean;
    teamReady: boolean;
    currentUser: string;
    summary: TeamSummary;
  }

  export const initialState: Estimations.State = {
    pickedTasks: [],
    availableTasks: [],
    teamEstimations: [],
    //   {
    //     pickedTasks: [
    //       {
    //         description: 'a',
    //         category: Category.BACKEND,
    //         storyPoints: 3,
    //       },
    //     ],
    //     storyPointsSum: 3,
    //     username: 'Mike',
    //   },
    //   {
    //     pickedTasks: [
    //       {
    //         description: 'lorem ipsum...',
    //         category: Category.BACKEND,
    //         storyPoints: 5,
    //       },
    //       {
    //         description: 'sdfasdgsdgsd sdgsdg',
    //         category: Category.BACKEND,
    //         storyPoints: 3,
    //       },
    //     ],
    //     storyPointsSum: 8,
    //     username: 'Marcus',
    //   },
    // ],
    ownEstimationSubmitted: false,
    teamReady: false,
    currentUser: null,
    summary: null,
  };
}

export namespace EstimationsReducer {
  export function reducer(
    state: Estimations.State = Estimations.initialState,
    action
  ): Estimations.State {
    switch (action.type) {
      case EstimationsActions.types.GetCurrentUserSuccess: {
        return {
          ...state,
          currentUser: action.payload,
        };
      }

      case EstimationsActions.types.LoadAvailableTasksSuccess: {
        return {
          ...state,
          availableTasks: action.payload.filter((task: Task) => !state.pickedTasks.includes(task)),
        };
      }

      case EstimationsActions.types.LoadPickedTasksSuccess: {
        return {
          ...state,
          pickedTasks: action.payload,
        };
      }

      case EstimationsActions.types.PickSuccess: {
        return {
          ...state,
          // ownEstimationSubmitted: false,
          pickedTasks: [...state.pickedTasks, action.payload],
          availableTasks: state.availableTasks.filter((task) => task.id !== action.payload.id),
        };
      }

      case EstimationsActions.types.RemovePickSuccess: {
        return {
          ...state,
          // ownEstimationSubmitted: false,
          availableTasks: [...state.availableTasks, action.payload],
          pickedTasks: state.pickedTasks.filter((task) => task.id !== action.payload.id),
        };
      }

      case EstimationsActions.types.LoadTeamEstimationsSuccess: {
        const ownEstimationSubmitted = action.payload
          .map((it: IndividualEstimation) => it.username)
          .includes(state.currentUser);
        // console.log('loading team estimations. Own submitted: ', ownEstimationSubmitted);
        return {
          ...state,
          teamEstimations: action.payload,
          ownEstimationSubmitted: ownEstimationSubmitted,
        };
      }

      case EstimationsActions.types.SubmitEstimationSuccess:
      case EstimationsActions.types.UpdateEstimationSuccess: {
        return {
          ...state,
          ownEstimationSubmitted: true,
          teamEstimations: [
            ...state.teamEstimations.filter((it) => it.username !== action.payload.username),
            action.payload,
          ],
        };
      }

      case EstimationsActions.types.ResetSubmissionSuccess: {
        return {
          ...state,
          ownEstimationSubmitted: false,
          teamReady: false,
          teamEstimations: state.teamEstimations.filter((it) => it.username != state.currentUser),
          pickedTasks: [],
          availableTasks: [...state.availableTasks, ...state.pickedTasks],
        };
      }

      case EstimationsActions.types.ResetAllSuccess: {
        return {
          ...state,
          ownEstimationSubmitted: false,
          teamReady: false,
          teamEstimations: [],
        };
      }

      case EstimationsActions.types.CheckTeamReadySuccess: {
        // console.log('team is finally ready: ', action.payload);
        return {
          ...state,
          teamReady: action.payload,
        };
      }

      case EstimationsActions.types.GetSummarySuccess: {
        return {
          ...state,
          summary: action.payload,
        };
      }

      default:
        return state;
    }
  }
}
