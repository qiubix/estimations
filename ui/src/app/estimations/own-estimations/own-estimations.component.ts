import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { map, take, tap, withLatestFrom } from 'rxjs/operators';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { Task } from 'src/app/tasks/tasks.model';
import { EstimationsActions } from '../store/estimations-actions';
import { Estimations } from '../store/estimations-reducer';

@Component({
  selector: 'app-own-estimations',
  templateUrl: './own-estimations.component.html',
  styleUrls: ['./own-estimations.component.css'],
})
export class OwnEstimationsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  disabled: boolean = false;

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    const userReady$ = this.store.pipe(select(AppSelectors.getUserReady));
    const pickedTasks$ = this.store.pipe(select(AppSelectors.getPickedTasksList));
    combineLatest([userReady$, pickedTasks$]).subscribe(([userReadyValue, pickedTasks]) => {
      this.disabled = pickedTasks.length === 0 || userReadyValue;
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onPick(task: Task): void {
    this.store.dispatch(new EstimationsActions.Pick(task));
  }

  onRemovePick(task: Task): void {
    this.store.dispatch(new EstimationsActions.RemovePick(task));
  }

  onReady(): void {
    const pickedTasks$ = this.store.pipe(select(AppSelectors.getPickedTasksList));
    pickedTasks$
      .pipe(
        // withLatestFrom(this.store.pipe(select(AppSelectors.getUserReady))),
        take(1),
        map((tasks) => {
          // console.log('input:', [tasks, userReady]);
          // console.log('output: ', [tasks.map((it) => it.id), userReady]);
          return tasks.map((it) => it.id);
        })
      )
      .subscribe((taskIds: number[]) => {
        // console.log('dispatching action with task ids: ', taskIds);
        // console.log('user is ready: ', userReady);
        // if (userReady) {
        // this.store.dispatch(new EstimationsActions.UpdateEstimation(taskIds));
        // } else {
        this.store.dispatch(new EstimationsActions.SubmitEstimation(taskIds));
        // }
      });
  }

  onReset(): void {
    this.store.dispatch(new EstimationsActions.ResetSubmission());
    // this.store.dispatch(new EstimationsActions.CheckTeamReady());
  }
}
