import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnEstimationsComponent } from './own-estimations.component';

describe('OwnEstimationsComponent', () => {
  let component: OwnEstimationsComponent;
  let fixture: ComponentFixture<OwnEstimationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OwnEstimationsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnEstimationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
