import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnSummaryComponent } from './own-summary.component';

describe('OwnSummaryComponent', () => {
  let component: OwnSummaryComponent;
  let fixture: ComponentFixture<OwnSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OwnSummaryComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
