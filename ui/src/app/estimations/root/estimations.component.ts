import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { TeamSummary } from '../estimations.model';
import { EstimationsActions } from '../store/estimations-actions';
import { Estimations } from '../store/estimations-reducer';

@Component({
  selector: 'app-estimations',
  templateUrl: './estimations.component.html',
  styleUrls: ['./estimations.component.scss'],
})
export class EstimationsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  teamSummary$: Observable<TeamSummary>;
  teamReady$: Observable<boolean>;

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new EstimationsActions.GetSummary());
    this.store.dispatch(new EstimationsActions.CheckTeamReady());
    this.teamSummary$ = this.store.pipe(select(AppSelectors.getTeamSummary));
    this.teamReady$ = this.store.pipe(select(AppSelectors.getWholeTeamReady));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
