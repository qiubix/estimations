import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { UserActions } from 'src/app/users/store/user-actions';
import { Users } from 'src/app/users/store/user-reducer';
import { AppSelectors } from '../store/app-selectors';

export interface Credentials {
  username: string;
  password: string;
}

export type AuthState = 'authenticated' | 'anonymous' | 'unknown';

@Injectable()
export class AuthService {
  private isLoggedIn = false;
  redirectUrl: string;
  private _currentLogin: string;

  logout(): void {
    this.isLoggedIn = false;
    this._currentLogin = null;
    localStorage.removeItem('currentUser');
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('currentUser');
  }

  constructor(private store: Store<Users.State>) {}

  authorize(credentials: Credentials): Observable<boolean> {
    const username = credentials.username;
    localStorage.setItem('currentUser', username);
    this.store.dispatch(new UserActions.Login(username));
    this.isLoggedIn = true;
    this._currentLogin = username;
    return of(true);
  }

  register(credentials: Credentials) {
    const username = credentials.username;
    this.store.dispatch(new UserActions.Register(username));
  }

  registerAndLogin(credentials: Credentials) {
    this.register(credentials);
    return this.authorize(credentials);
  }

  get availableUsers(): Observable<string[]> {
    this.store.dispatch(new UserActions.LoadList());
    return this.store.pipe(
      select(AppSelectors.getAllUsers),
      map((users) => users.map((it) => it.username)),
      filter((it) => it.length > 0),
      take(1)
    );
  }

  get currentLogin(): string {
    return localStorage.getItem('currentUser');
  }
}
