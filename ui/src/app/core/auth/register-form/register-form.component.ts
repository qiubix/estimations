import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css'],
})
export class RegisterFormComponent implements OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  form: FormGroup;

  @Output() registeredUsername: EventEmitter<string> = new EventEmitter<string>();

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
    });
  }

  register(): void {
    this.registeredUsername.emit(this.form.value);
  }

  cancel(): void {
    this.registeredUsername.emit();
  }
}
