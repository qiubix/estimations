import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksAddEditDialogComponent } from './tasks-add-edit-dialog.component';

describe('TasksAddEditDialogComponent', () => {
  let component: TasksAddEditDialogComponent;
  let fixture: ComponentFixture<TasksAddEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TasksAddEditDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksAddEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
