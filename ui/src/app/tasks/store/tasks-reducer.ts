import { Category, Task } from '../tasks.model';
import { TasksActions } from './tasks-actions';

export namespace Tasks {
  export interface State {
    tasksList: Task[];
  }

  export const initialState: Tasks.State = {
    tasksList: [],
  };
}

export namespace TasksReducer {
  export function reducer(state: Tasks.State = Tasks.initialState, action): Tasks.State {
    switch (action.type) {
      case TasksActions.types.LoadListSuccess: {
        return {
          ...state,
          tasksList: action.payload,
        };
      }

      case TasksActions.types.CreateSuccess: {
        return {
          ...state,
          tasksList: [...state.tasksList, action.payload],
        };
      }

      case TasksActions.types.CreateManySuccess: {
        return {
          ...state,
          tasksList: [...state.tasksList].concat(action.payload),
        };
      }

      case TasksActions.types.UpdateSuccess: {
        return {
          ...state,
          tasksList: state.tasksList.map((task: Task) =>
            task.id === action.payload.id ? action.payload : task
          ),
        };
      }

      case TasksActions.types.DeleteSuccess: {
        return {
          ...state,
          tasksList: state.tasksList.filter((task: Task) => task.id !== action.payload),
        };
      }

      default:
        return state;
    }
  }
}
