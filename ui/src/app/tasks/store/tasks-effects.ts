import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { TasksService } from '../tasks.service';
import { TasksActions } from './tasks-actions';

import CreateSuccess = TasksActions.CreateSuccess;
import UpdateSuccess = TasksActions.UpdateSuccess;
import DeleteSuccess = TasksActions.DeleteSuccess;
import LoadListSuccess = TasksActions.LoadListSuccess;
import CreateManySuccess = TasksActions.CreateManySuccess;
import FailedAction = TasksActions.Fail;

@Injectable()
export class TaskEffects {
  @Effect()
  LoadAllTasks: Observable<Action> = this.actions.pipe(
    ofType(TasksActions.types.LoadList),
    switchMap(() =>
      this.service.getAll().pipe(
        map((data) => new LoadListSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  CreateTask: Observable<Action> = this.actions.pipe(
    ofType(TasksActions.types.Create),
    switchMap((action: TasksActions.Create) =>
      this.service.addNew(action.payload).pipe(
        map((data) => new CreateSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  CreateMany: Observable<Action> = this.actions.pipe(
    ofType(TasksActions.types.CreateMany),
    switchMap((action: TasksActions.CreateMany) =>
      this.service.addMany(action.payload).pipe(
        map((data) => new CreateManySuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  UpdateTask: Observable<Action> = this.actions.pipe(
    ofType(TasksActions.types.Update),
    switchMap((action: TasksActions.Update) =>
      this.service.update(action.payload).pipe(
        map((data) => new UpdateSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  DeleteTask: Observable<Action> = this.actions.pipe(
    ofType(TasksActions.types.Delete),
    switchMap((action: TasksActions.Delete) =>
      this.service.delete(action.payload).pipe(
        map((data) => new DeleteSuccess(action.payload)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  constructor(private actions: Actions, private service: TasksService) {}
}
