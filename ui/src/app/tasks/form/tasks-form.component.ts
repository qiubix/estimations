import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Category, Task } from '../tasks.model';

@Component({
  selector: 'app-tasks-form',
  templateUrl: './tasks-form.component.html',
  styleUrls: ['./tasks-form.component.scss'],
})
export class TasksFormComponent implements OnInit {
  @Input() task: Task;

  @Output() formOutput: EventEmitter<Task> = new EventEmitter<Task>();

  form: FormGroup;
  categories = Category;
  categoryKeys = Object.keys(this.categories);

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      description: [this.task ? this.task.description : ''],
      category: [this.task ? this.task.category : ''],
      storyPoints: [this.task ? this.task.storyPoints : 0, Validators.pattern(/(0|1|2|3|5|8|13)/)],
    });
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }
    this.formOutput.emit(this.form.value);
  }
}
