import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { AppSelectors } from '../core/store/app-selectors';
import { TasksActions } from './store/tasks-actions';
import { Tasks } from './store/tasks-reducer';

@Injectable()
export class TasksListGuard implements CanActivate {
  constructor(private store: Store<Tasks.State>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log('Trying to activate task list');
    return this.getFromStoreOrAPI().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getAllTasksList),
      tap((tasks) => {
        console.log('current tasks: ', tasks);
        if (!tasks) {
          console.log('dispatching load list action');
          this.store.dispatch(new TasksActions.LoadList());
        }
      }),
      filter((tasks) => tasks != null && tasks.length > 0),
      take(1)
    );
  }
}
