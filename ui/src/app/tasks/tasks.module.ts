import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksComponent } from './root/tasks.component';
import { TasksAddEditDialogComponent } from './add-edit-dialog/tasks-add-edit-dialog.component';
import { TasksListComponent } from './list/tasks-list.component';
import { TasksFormComponent } from './form/tasks-form.component';
import { SharedModule } from '../shared/shared.module';
import { TasksService } from './tasks.service';
import { TasksListGuard } from './tasks.guard';

@NgModule({
  declarations: [
    TasksComponent,
    TasksListComponent,
    TasksAddEditDialogComponent,
    TasksFormComponent,
  ],
  imports: [SharedModule, TasksRoutingModule],
  exports: [TasksComponent],
  providers: [TasksService, TasksListGuard],
})
export class TasksModule {}
