import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Category } from 'src/app/tasks/tasks.model';
import { TasksFilter } from './task-filter.util';

@Component({
  selector: 'app-tasks-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class TasksFilterComponent implements OnInit, OnDestroy {
  @Output() filterChange: EventEmitter<TasksFilter> = new EventEmitter<TasksFilter>();

  form: FormGroup;
  categories = Category;
  categoryKeys = Object.keys(this.categories);

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private fb: FormBuilder) {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      description: '',
      category: null,
    });
    this.form.valueChanges
      .pipe(debounceTime(250), distinctUntilChanged(), takeUntil(this.ngUnsubscribe))
      .subscribe((value: TasksFilter) => this.filterChange.emit(value));
  }
}
