import { environment } from './environments/environment';

export const Config = {
  endpoints: {
    tasksModule: environment.apiUrl + '/tasks',
    estimationsModule: environment.apiUrl + '/estimations',
    usersModule: environment.apiUrl + '/users',
  },
};
