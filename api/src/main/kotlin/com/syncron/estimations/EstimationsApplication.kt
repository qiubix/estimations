package com.syncron.estimations

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

const val API_URI = "/api"

@SpringBootApplication
class EstimationsApplication

fun main(args: Array<String>) {
	runApplication<EstimationsApplication>(*args)
}
