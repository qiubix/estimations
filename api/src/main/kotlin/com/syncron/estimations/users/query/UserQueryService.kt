package com.syncron.estimations.users.query

import com.syncron.estimations.users.domain.UserReadRepository
import org.springframework.transaction.annotation.Transactional

@Transactional
class UserQueryService(
    private val userReadRepository: UserReadRepository
) {

    fun findBy(username: String) = userReadRepository.findBy(username)

    fun findAll() = userReadRepository.findAll()

    fun findAllActiveUsers() = userReadRepository.findAll().filter { it.active }

}
