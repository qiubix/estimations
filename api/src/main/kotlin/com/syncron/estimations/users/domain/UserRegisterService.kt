package com.syncron.estimations.users.domain

import org.springframework.transaction.annotation.Transactional

@Transactional
class UserRegisterService(
    private val userWriteRepository: UserWriteRepository
) {
    fun register(username: String): User {
        val user = User(
            username = username,
            active = false
        )
        return userWriteRepository.create(user)
    }
}
