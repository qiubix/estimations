package com.syncron.estimations.users.domain

import org.springframework.transaction.annotation.Transactional

@Transactional
class UserLoginService(
    private val userReadRepository: UserReadRepository,
    private val userWriteRepository: UserWriteRepository
) {

    fun authenticate(username: String): User {
        val user = userReadRepository.findBy(username) ?: throw UserNotFoundException(username)
        return userWriteRepository.save(user.copy(active = true))
    }

}
