package com.syncron.estimations.users.infrastructure

import com.syncron.estimations.shared.infrastructure.selectSingleOrNull
import com.syncron.estimations.shared.infrastructure.updateExactlyOne
import com.syncron.estimations.users.domain.User
import com.syncron.estimations.users.domain.UserReadRepository
import com.syncron.estimations.users.domain.UserWriteRepository
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll


object UsersTable: Table("users") {

    val username = text("username")
    val active = bool("active")

    override val primaryKey = PrimaryKey(username, name = "UsersTablePKConstraint")

}

fun ResultRow.toUser(): User = User(
    username = this[UsersTable.username],
    active = this[UsersTable.active]
)

class SqlUserReadRepository : UserReadRepository {

    override fun findBy(username: String): User? =
        UsersTable.selectSingleOrNull { UsersTable.username eq username }?.toUser()

    override fun findAll(): List<User> = UsersTable.selectAll().map { it.toUser() }

}

class SqlUserWriteRepository: UserWriteRepository {

    override fun create(user: User): User {
        UsersTable.insert {
            it[username] = user.username
            it[active] = user.active
        }
        return user
    }

    override fun save(user: User): User {
        UsersTable.updateExactlyOne({ UsersTable.username eq user.username }) {
            it[username] = user.username
            it[active] = user.active
        }
        return user
    }

}
