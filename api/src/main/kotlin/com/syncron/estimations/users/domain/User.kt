package com.syncron.estimations.users.domain

data class User(
    val username: String,
    val active: Boolean
)
