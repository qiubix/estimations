package com.syncron.estimations.users.domain

import com.syncron.estimations.shared.domain.ApplicationException

interface UserReadRepository {

    fun findBy(username: String): User?

    fun getBy(username: String) = findBy(username) ?: throw UserNotFoundException(username)

    fun findAll(): List<User>

}

interface UserWriteRepository {
    fun create(user: User): User
    fun save(user: User): User
}

class UserNotFoundException(username: String) : ApplicationException("User with username $username not found")
