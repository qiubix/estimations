package com.syncron.estimations.users.domain

import com.syncron.estimations.users.endpoint.UserDto
import org.springframework.transaction.annotation.Transactional

@Transactional
class UserUpdateService(
    private val userReadRepository: UserReadRepository,
    private val userWriteRepository: UserWriteRepository
) {

    fun update(user: UserDto): User {
        return userReadRepository.getBy(user.username)
            .copy(active = user.active)
            .also(userWriteRepository::save)
    }

}
