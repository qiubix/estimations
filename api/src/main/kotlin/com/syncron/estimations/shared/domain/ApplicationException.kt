package com.syncron.estimations.shared.domain

open class ApplicationException(message: String): RuntimeException(message)
