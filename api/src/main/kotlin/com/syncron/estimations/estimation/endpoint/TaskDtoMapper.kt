package com.syncron.estimations.estimation.endpoint

import com.syncron.estimations.estimation.domain.model.Task

fun buildFrom(taskDto: TaskDto) = Task(
    id = taskDto.id,
    description = taskDto.description,
    storyPoints = taskDto.storyPoints,
    category = taskDto.category
)

fun buildFrom(createTaskDto: CreateTaskDto) = Task(
    description = createTaskDto.description,
    storyPoints = createTaskDto.storyPoints,
    category = createTaskDto.category
)

fun Task.toDto() = TaskDto(
    id = this.id,
    description = this.description,
    category = this.category,
    storyPoints = this.storyPoints
)
