package com.syncron.estimations.estimation.query

import com.syncron.estimations.estimation.domain.ports.EstimationReadRepository
import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.model.TeamSummary
import com.syncron.estimations.users.query.UserQueryService

class EstimationQueryService(
    private val estimationReadRepository: EstimationReadRepository,
    private val userQueryService: UserQueryService
) {

    fun getAllEstimations(): List<IndividualEstimation> = estimationReadRepository.findAll()

    fun isWholeTeamReady(): Boolean {
        val activeUsers = userQueryService.findAllActiveUsers().map { it.username }
        return getAllEstimations().map { it.username }.toSet() == activeUsers.toSet()
    }

    fun getSummary(): TeamSummary? {
        return if (isWholeTeamReady()) {
            TeamSummary(getAllEstimations())
        } else {
            null
        }
    }

}
