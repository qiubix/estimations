package com.syncron.estimations.estimation.domain.model


class TeamSummary(
    private val estimations: List<IndividualEstimation>
) {

    fun average(): Double = estimations.map { it.storyPointsSum() }.sum().toDouble().div(estimations.size)

    fun minimum(): Int = estimations.map { it.storyPointsSum() }.minOrNull() ?: 0

    fun maximum(): Int = estimations.map { it.storyPointsSum() }.maxOrNull() ?: 0

    fun numberOfEstimations(): Int = estimations.size

    fun commonTasks(): Set<Task> {
        if (estimations.isEmpty()) {
            return emptySet()
        }
        return estimations
            .map { it.pickedTasks }
            .reduce { acc, it -> (it intersect acc).toList() }
            .toSet()
    }

    private fun tasksUnion(): Set<Task> = estimations.flatMap { it.pickedTasks }.toSet()

    fun pickSummaries(): List<PickSummary> {
        return tasksUnion()
            .map { task ->
                PickSummary(
                    taskId = task.id,
                    description = task.description,
                    category = task.category,
                    storyPoints = task.storyPoints,
                    allVoted = estimations.filter { it.pickedTasks.contains(task) }.count() == estimations.size,
                    whoVoted = estimations.filter { it.pickedTasks.contains(task) }.map { it.username }
                )
            }
    }

}
