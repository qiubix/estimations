package com.syncron.estimations.estimation.domain.model

data class PickSummary(
    val taskId: Long,
    val description: String,
    val category: String,
    val storyPoints: Int,
    val allVoted: Boolean,
    val whoVoted: List<String>
)
