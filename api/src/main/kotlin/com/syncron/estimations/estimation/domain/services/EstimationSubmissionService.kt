package com.syncron.estimations.estimation.domain.services

import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.ports.EstimationWriteRepository

class EstimationSubmissionService(
    private val estimationWriteRepository: EstimationWriteRepository
) {

    fun submitEstimation(estimation: IndividualEstimation): IndividualEstimation {
        return estimationWriteRepository.add(estimation)
    }

    fun updateEstimation(estimation: IndividualEstimation): IndividualEstimation {
        return estimationWriteRepository.update(estimation)
    }

}
