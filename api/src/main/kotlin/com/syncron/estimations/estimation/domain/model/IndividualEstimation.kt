package com.syncron.estimations.estimation.domain.model


data class IndividualEstimation(
    val username: String,
    val pickedTasks: List<Task> = emptyList()
) {

    fun storyPointsSum(): Int = pickedTasks.map { it.storyPoints }.sum()

    fun getPickedTaskIds(): List<Long> = pickedTasks.map { it.id }

}
