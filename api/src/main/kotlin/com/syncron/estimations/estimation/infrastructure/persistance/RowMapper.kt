package com.syncron.estimations.estimation.infrastructure.persistance

import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.model.Task
import org.jetbrains.exposed.sql.ResultRow


fun ResultRow.toTask(): Task = Task(
    id = this[TasksTable.id],
    description = this[TasksTable.description],
    category = this[TasksTable.category],
    storyPoints = this[TasksTable.storyPoints],
)

fun ResultRow.toIndividualEstimation(): IndividualEstimation = IndividualEstimation(
    username = this[IndividualEstimationsTable.username]
)

fun Iterable<ResultRow>.toTasksMap(): Map<Long, Task> {
    val map = mutableMapOf<Long, Task>()
    this.forEach { resultRow ->
        val taskId = resultRow.getOrNull(TasksTable.id) ?: return@forEach
        val task = map[taskId] ?: resultRow.toTask()
        map[taskId] = task
    }
    return map
}

fun Iterable<ResultRow>.toEstimationList(): List<IndividualEstimation> {
    val estimationsMap = mutableMapOf<String, IndividualEstimation>()
    val pickedTasks = mutableMapOf<String, MutableSet<Task>>()
    val tasksMap = this.toTasksMap()
    this.forEach { resultRow ->
        val estimationId = resultRow.getOrNull(IndividualEstimationsTable.username) ?: return@forEach
        val estimation = estimationsMap[estimationId] ?: resultRow.toIndividualEstimation()
        estimationsMap[estimationId] = estimation
        resultRow.getOrNull(TasksTable.id)?.let {
            val tasks = tasksMap.filter { task -> task.key == it }.values.toMutableSet()
            pickedTasks[estimationId]?.addAll(tasks) ?: pickedTasks.put(estimationId, tasks)
        }
    }
    return estimationsMap.mapValues { (_, estimation) ->
        estimation.copy(
            pickedTasks = pickedTasks[estimation.username]?.toList() ?: emptyList()
        )
    }.values.toList()
}
