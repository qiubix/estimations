package com.syncron.estimations.estimation.endpoint

import com.syncron.estimations.API_URI
import com.syncron.estimations.estimation.domain.model.Task
import com.syncron.estimations.estimation.domain.services.TaskCreateService
import com.syncron.estimations.estimation.domain.services.TaskUpdateService
import com.syncron.estimations.estimation.query.TaskQueryService
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("$API_URI${TasksEndpoint.ROOT_PATH}")
@Transactional
class TasksEndpoint(
    private val taskQueryService: TaskQueryService,
    private val taskCreateService: TaskCreateService,
    private val taskUpdateService: TaskUpdateService
) {

    companion object {
        const val ROOT_PATH = "/tasks"
    }

    @GetMapping
    fun getAllTasks() = taskQueryService.getAll().map { it.toDto() }

    @PostMapping
    fun createTask(@RequestBody createTaskDto: CreateTaskDto): TaskDto = taskCreateService.createNewTask(buildFrom(createTaskDto)).toDto()

    @PostMapping("/import")
    fun addMany(@RequestBody taskDtos: List<CreateTaskDto>): List<TaskDto> {
        val newTasks = taskDtos.map(::buildFrom)
        return taskCreateService.createManyTasks(newTasks).map(Task::toDto)
    }

    @PutMapping
    fun updateTask(@RequestBody taskDto: TaskDto): TaskDto = taskUpdateService.updateTask(buildFrom(taskDto)).toDto()

    @DeleteMapping("/{taskId}")
    fun deleteTask(@PathVariable taskId: Long) = taskUpdateService.deleteTask(taskId)
}
