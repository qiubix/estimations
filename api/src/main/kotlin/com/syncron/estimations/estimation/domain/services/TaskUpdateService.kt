package com.syncron.estimations.estimation.domain.services

import com.syncron.estimations.estimation.domain.model.Task
import com.syncron.estimations.estimation.domain.ports.TaskReadRepository
import com.syncron.estimations.estimation.domain.ports.TaskWriteRepository

class TaskUpdateService(
    private val taskWriteRepository: TaskWriteRepository,
    private val taskReadRepository: TaskReadRepository
) {

    fun updateTask(task: Task): Task {
        val taskToUpdate = taskReadRepository.findBy(task.id) ?: throw IllegalStateException("Task does not exist.")
        return taskToUpdate
            .copy(
                description = task.description,
                category = task.category,
                storyPoints = task.storyPoints
            )
            .also { taskWriteRepository.save(it) }
    }

    fun deleteTask(taskId: Long) {
        taskWriteRepository.delete(taskId)
    }

}
