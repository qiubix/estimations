package com.syncron.estimations.estimation.domain.services

import com.syncron.estimations.estimation.domain.model.Task
import com.syncron.estimations.estimation.domain.ports.TaskReadRepository
import com.syncron.estimations.estimation.domain.ports.TaskWriteRepository

class TaskCreateService(
    private val taskReadRepository: TaskReadRepository,
    private val taskWriteRepository: TaskWriteRepository
) {

    fun createNewTask(newTask: Task): Task {
        val nextId = taskReadRepository.findAll().map { it.id }.maxOrNull()?.plus(1L) ?: 1L
        val taskWithId = newTask.copy(id = nextId)
        return taskWriteRepository.create(taskWithId)
    }

    fun createManyTasks(tasks: List<Task>): List<Task> {
        return tasks.map { createNewTask(it) }
    }

}
