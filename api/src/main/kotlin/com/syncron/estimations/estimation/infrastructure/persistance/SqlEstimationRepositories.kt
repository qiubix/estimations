package com.syncron.estimations.estimation.infrastructure.persistance

import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.ports.EstimationReadRepository
import com.syncron.estimations.estimation.domain.ports.EstimationWriteRepository
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll

class SqlEstimationReadRepository : EstimationReadRepository {

    companion object {
        val IndividualEstimationWithTasks = (IndividualEstimationsTable leftJoin  PickedTasksTable leftJoin TasksTable)
    }

    override fun findUserEstimation(username: String): IndividualEstimation? =
        IndividualEstimationWithTasks
            .select { IndividualEstimationsTable.username eq username }
            .toEstimationList()
            .singleOrNull()

    override fun findAll(): List<IndividualEstimation> =
        IndividualEstimationWithTasks
            .selectAll()
            .toEstimationList()

}

class SqlEstimationWriteRepository : EstimationWriteRepository {

    override fun add(estimation: IndividualEstimation): IndividualEstimation {
        IndividualEstimationsTable.insert {
            it[username] = estimation.username
        }
        estimation.pickedTasks.forEach { task ->
            PickedTasksTable.insert {
                it[taskId] = task.id
                it[individualEstimationId] = estimation.username
            }
        }
        return estimation
    }

    override fun update(estimation: IndividualEstimation): IndividualEstimation {
        PickedTasksTable.deleteWhere { PickedTasksTable.individualEstimationId eq estimation.username }
        estimation.pickedTasks.forEach { task ->
            PickedTasksTable.insert {
                it[taskId] = task.id
                it[individualEstimationId] = estimation.username
            }
        }
        return estimation
    }

    override fun deleteByUsername(username: String) {
        PickedTasksTable.deleteWhere { PickedTasksTable.individualEstimationId eq username }
        IndividualEstimationsTable.deleteWhere { IndividualEstimationsTable.username eq username }
    }

    override fun deleteAll() {
        PickedTasksTable.deleteAll()
        IndividualEstimationsTable.deleteAll()
    }

}
