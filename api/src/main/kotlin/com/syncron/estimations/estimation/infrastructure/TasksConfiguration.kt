package com.syncron.estimations.estimation.infrastructure

import com.syncron.estimations.estimation.domain.services.TaskCreateService
import com.syncron.estimations.estimation.domain.ports.TaskReadRepository
import com.syncron.estimations.estimation.domain.services.TaskUpdateService
import com.syncron.estimations.estimation.domain.ports.TaskWriteRepository
import com.syncron.estimations.estimation.infrastructure.persistance.SqlTaskReadRepository
import com.syncron.estimations.estimation.infrastructure.persistance.SqlTaskWriteRepository
import com.syncron.estimations.estimation.query.TaskQueryService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.syncron.estimations.estimation"])
class TasksConfiguration {

    @Bean
    fun taskCreateService(taskWriteRepository: TaskWriteRepository,
                          taskReadRepository: TaskReadRepository
    ): TaskCreateService = TaskCreateService(taskReadRepository, taskWriteRepository)

    @Bean
    fun taskUpdateService(
        taskWriteRepository: TaskWriteRepository,
        taskReadRepository: TaskReadRepository
    ): TaskUpdateService = TaskUpdateService(taskWriteRepository, taskReadRepository)

    @Bean
    fun taskQueryService(taskReadRepository: TaskReadRepository) = TaskQueryService(taskReadRepository)

    @Bean
    fun taskReadRepository(): TaskReadRepository = SqlTaskReadRepository()

    @Bean
    fun taskWriteRepository(): TaskWriteRepository = SqlTaskWriteRepository()
}
