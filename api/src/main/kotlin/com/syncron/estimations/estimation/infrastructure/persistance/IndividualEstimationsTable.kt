package com.syncron.estimations.estimation.infrastructure.persistance

import com.syncron.estimations.users.infrastructure.UsersTable
import org.jetbrains.exposed.sql.Table

object IndividualEstimationsTable : Table("estimations") {
    val username = text("username")

    override val primaryKey = PrimaryKey(UsersTable.username, name = "EstimationsTablePKConstraint")
}

object PickedTasksTable: Table("picked_tasks") {
    val taskId = reference("task_id", TasksTable.id)
    val individualEstimationId = reference("estimation_id", IndividualEstimationsTable.username)
}
